def decimal_to_octal(n):

	oct = [0] * 100;

	i = 0;
	while (n != 0):
		oct[i] = n % 8;
		n = int(n/8);
		i += 1;

	for j in range(i - 1, -1, -1):
		print(oct[j], end = "");

n = int(input('Enter your number:' ))

decimal_to_octal(n)