import urllib.request as req
import json

url = 'https://api.shodan.io/shodan/host/45.79.99.249?key=WX78rxeneprr3Grp0M0PeYacfVbgwf6S'

json_obj = req.urlopen(url)
json_data = json.load(json_obj)

for item in json_data['data']:
    print("HASH is: " + str(item['hash']))
    print("IP is: " + str(item['ip']))
    print("Organization is: " + str(item['org']))
    print("ISP is: " + str(item['isp']))
    print("Protocol is: " + str(item['transport']))
    print("ASN no: " + str(item['asn']))
    print("Port no: " + str(item['port']))
    print("Hostname: " + str(item['hostnames']))

print()

for data in json_data['data']:
    a = (data['_shodan'])
    id = a['id']
    crawler = a['crawler']
    print("ID is: " + str(id))
    print("Crawler: " + str(crawler))

