from urllib.request import urlopen as req
from bs4 import BeautifulSoup as soup

url = 'https://www.amazon.in/s?k=leadership+books&crid=3QD8CZJPWUQ4F&sprefix=leadersh%2Caps%2C279&ref=nb_sb_ss_organic-diversity_3_8'

r = req(url)
html_page = r.read()
r.close()

s = soup(html_page, "html.parser")

fetch = s.findAll("div",{"class":"s-include-content-margin s-border-bottom s-latency-cf-section"})
title = fetch[0].h2.span.text

author = s.findAll("div",{"class":"a-row a-size-base a-color-secondary"})
author_name = author[0].text

ratings = s.findAll("div",{"class":"a-row a-size-small"})
rating = ratings[0].span.text.strip()

cateogry = s.findAll("div",{"class":"a-row a-size-base a-color-base"})
typeof = cateogry[0].text.strip()

price_paperback = s.findAll("a",{"class":"a-size-base a-link-normal s-no-hover a-text-normal"})
price = price_paperback[0].span.span.text

print("Title: " + title)
print("Author Name: "+ author_name)
print("Ratings: "+ rating)
print("Type: " + typeof)
print("Price: "+ price)