import flask
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

books = [
    {'id': 0,
     'name': 'Cybersecurity Essentials',
     'Author': ' Charles J. Brooks',
     'Date': 'Oct 5, 2018'},
    {'id': 1,
     'name': 'Cybersecurity: The Beginners Guide: A comprehensive guide to getting started in cybersecurity',
     'Author': 'Dr. Erdal Ozkaya',
     'Date': 'May 27, 2019'},
    {'id': 2,
     'name': 'A Leaders Guide to Cybersecurity: Why Boards Need to Lead--and How to Do It',
     'Author': ' Thomas J. Parenty and Jack J. Domet',
     'Date': 'Dec 3, 2019'}
]

@app.route('/api/v1/resources/books/all', methods=['GET'])
def api_all():
    return jsonify(books)

app.run()
