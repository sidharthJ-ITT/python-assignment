import urllib.request as req
import json

url = 'http://127.0.0.1:5000/api/v1/resources/books/all'

json_obj = req.urlopen(url)
json_data = json.load(json_obj)

for i in range(0,3):
    data = json_data[i]
    for item in data:
        print("ID: " + str(data['id']))
        print("Name: " + str(data['name']))
        print("Author:" + str(data['Author']))
        print("Date: " + str(data['Date']))
        print()
        break
