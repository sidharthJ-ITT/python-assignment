def octal_to_decimal(n):

	num = n;
	dec = 0;

	b = 1;

	t = num;
	while(t):

		l = t % 10;
		t = int(t / 10);

		dec += l * b;
		b = b * 8;

	return dec;

num = int(input('Please enter number: '))
print(octal_to_decimal(num));